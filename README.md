# Entrega Práctica

Práctica final curso 2021-2022 (MisPalabras)

## Datos

* Nombre: Gonzalo García-Augustín García
* Titulación: Ingeniería en Telemática
* Nombre de los laboratorios: augustin
* Nombre de **GitLab**: augustin
* Video básico (url): https://youtu.be/oDPUSAd-Cc4
* Video de la parte opcional (url): https://youtu.be/fHKIKsBs3q8
* Despliegue (url): http://gogarciaaugust2018.pythonanywhere.com/

## Cuenta Admin Site

* admin/admin
* admin2/admin2

## Cuentas usuarios

* admin/admin
* admin2/admin2

## Resumen de la parte obligatoria

La parte obligatoria ha sido incluida en su totalidad. La prueba de funcionamiento se adjunto 
en el video básico incluido más arriba.

## Lista partes opcionales

* Inclusión de un *favicon* del sitio. 
* Visualización de la página de palabra en formato JSON y/o XML, de forma
similar a como se ha indicado para la página principal.
* Generación de un documento XML y/o JSON para los comentarios puestos
en el sitio.

## Particularidades

Me gustaría comentar que en la página http://gogarciaaugust2018.pythonanywhere.com/ no se podrá disfrutar con total libertad las posibilidades
que se ofrecen en esta práctica ya que https://www.pythonanywhere.com/ no permite almacenar contenido de la RAE, y eso puede causar controversias
en el despliegue de la aplcación web.
