
from . import views
from django.urls import path

urlpatterns = [
    path('', views.index),
    path('user', views.user_page),
    path('format=xml', views.palabras_xml),
    path('format=json', views.palabras_json),
    path('<str:palabra>/format=xml', views.palabra_xml),
    path('<str:palabra>/format=json', views.palabra_json),
    path('<str:palabra>/comentarios/format=xml', views.comentarios_xml),
    path('<str:palabra>/comentarios/format=json', views.comentarios_json),
    path('ayuda', views.ayuda_page),
    path('<str:palabra>', views.palabra_page),
    path('<str:palabra>/andy', views.andy),
    path('<str:palabra>/duck', views.duck),
    path('<str:palabra>/guy', views.guy),
    path('<str:palabra>/albert', views.albert),
    path('<str:palabra>/baby', views.baby),
    path('<str:palabra>/doge', views.doge),
]