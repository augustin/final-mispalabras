from django.db import models

# Create your models here.

class Palabra(models.Model):
    titulo = models.CharField(max_length=256)
    fecha = models.DateTimeField(auto_now=True)
    usuario = models.CharField(max_length=256)
    votos = models.IntegerField(default=0)
    descripcion = models.TextField(blank=True)
    imagen = models.URLField(max_length=256)

    def __str__(self):
        return self.usuario + " = " + str(self.id)

class Comentario(models.Model):
    contenido = models.TextField(blank=False)
    fecha = models.DateTimeField(auto_now=True)
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    usuario = models.CharField(max_length=256)

class Voto(models.Model):
    voted = models.BooleanField(default=False)
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    usuario = models.CharField(max_length=256)

class Enlace(models.Model):
    enlace = models.URLField(max_length=256)
    contenido = models.TextField(blank=False)
    titulo = models.CharField(max_length=256)
    imagen = models.URLField(max_length=256)
    fecha = models.DateTimeField(auto_now=True)
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    usuario = models.CharField(max_length=256)

class RAE(models.Model):
    fecha = models.DateTimeField(auto_now=True)
    palabra = models.CharField(max_length=256)
    usuario = models.CharField(max_length=256)
    descripcion = models.TextField(blank=False)