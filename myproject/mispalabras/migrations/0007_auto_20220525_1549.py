# Generated by Django 3.1.7 on 2022-05-25 15:49

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mispalabras', '0006_voto_voted'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='palabra',
            name='votos',
        ),
        migrations.AddField(
            model_name='palabra',
            name='votos',
            field=models.ManyToManyField(blank=True, related_name='votes', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='Voto',
        ),
    ]
