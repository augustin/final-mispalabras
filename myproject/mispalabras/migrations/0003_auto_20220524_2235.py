# Generated by Django 3.1.7 on 2022-05-24 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mispalabras', '0002_auto_20220524_2220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comentario',
            name='usuario',
            field=models.CharField(max_length=256),
        ),
        migrations.AlterField(
            model_name='enlace',
            name='usuario',
            field=models.CharField(max_length=256),
        ),
    ]
