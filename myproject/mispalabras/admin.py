from django.contrib import admin
from .models import Palabra, Comentario

# Register your models here.
admin.site.register(Palabra)
admin.site.register(Comentario)